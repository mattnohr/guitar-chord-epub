# Contributing

## Welcome

Thank you for wanting to contribute to this project!

## Issues

If you find any issues, please file a [bug in GitLab](https://gitlab.com/mattnohr/guitar-chord-epub/-/issues)

## Development

There is no `master` branch in this repository. Instead, use the `main` branch.

Since the `main` branch is protected, you will need to create merge requests to change the code.