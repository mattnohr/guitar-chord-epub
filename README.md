# Guitar Chords Epub

![Pipeline Status Badge](https://gitlab.com/mattnohr/guitar-chord-epub/badges/main/pipeline.svg)
![Coverage Badge](https://gitlab.com/mattnohr/guitar-chord-epub/badges/main/coverage.svg)

The goal of this project is to create e-books in the .epub format that contain guitar music with chords.

## Background / Problem

I started using an Amazon Kindle to store my guitar music so I did not have to carry around big books of music. However, I could not find a good format for the songs. 

Here are the problems I faced:

- Using **a PDF file** would show up correctly, however the text could get very small trying to fit an entire PDF page on the Kindle screen. I also had to spend time formatting a document (usually a Google Doc) to fit the music in, and then export it to a PDF.
- Using **MOBI/EPUB format** or other ebook format did not work because the lyrics would not wrap correctly with the chords that were above them. For example, when the text wraps you could get something like this:

```
       D
   A
'Cause every little thing gonna 
be alright
```

When I expect it to be like this:

```
       D
'Cause every little thing gonna 
     A
be alright
```

## Goals

My goal was to then make an ebook that met these goals:

1. The lyrics and chords were readable and not very small
1. I could increase/decrease the size of the font using the Kindle's built-in features
1. The chords would always stick above the correct lyrics, even when the text wraps
1. (Less important) I did not want to spend a lot of time formatting a text document to make the layout look correct

## How This Works

Generating EPUB books is basically an HTML file with custom styles. 

For this music, I treat the chord symbols as part of the same word as the lyrics, but I just apply a custom style. They CSS style moves the chord symbol above the lyric. However, since it seems like (to HTML) part of the same word, when the ebook reader wraps the text it will keep the chord with the lyric.

## Usage

1. Create a .yml file with you song. There is an example in the `songs` directory
1. `./gradlew run -Psong=/path/to/song.yml` (use `gradlew.bat` on Windows)
1. The first time you run the program it may take a bit to download dependencies, but after that it should be very fast.
1. The generated epub file will be in the `output` directory. If you want to change this, you can pass a new param with `-Poutput=/path/to/output/directory` (currently does not work with paths that contain `~`)

### Optionally Convert to AZW3

If you are going to read this on a Kindle, you should convert it to an **AZW3** file, **not a MOBI** file. The AZW3 format seems to keep the styles much better than MOBI. 

I currently use [Calibre](https://calibre-ebook.com/) to do this conversion. You can use the [command line interface](https://manual.calibre-ebook.com/generated/en/ebook-convert.html) that comes with Calibre if you want.

On a Mac it would look something like this:

```bash
/Applications/calibre.app/Contents/MacOS/ebook-convert input.epub output.azw3
```

## Song Format

The song should be a valid YAML file. If it is invalid YAML it will probably break! Use something like `yamllint` to make sure it is valid.

The required keys in the YAML file are `title`, `artist`, and `chords`.  Optionally you can add a `capo` key if the song needs a capo and that will be displayed at the top of the output.

`chords` is a multiline entry with the lyrics and chords embedded together. Use a format like `{G}` or `{Am7}` for the chords. The chord name should go directly after, and on the same line as, the letter of the lyric it should appear above.

If you use [Calibre](https://calibre-ebook.com/) and want to add the album/track information as series data in Calibre, you can also add `album` and `track` as keys in the yml.

There is also an experimental `transpose` key that will change all the chords up or down.

### Spacing 

If you need to have chords over a blank space, such as at the end of a line, then make sure you add enough blank spaces to allow for the full chord name, otherwise they may end up overlapping.

For example this will allow the `Am` chord enough space before the `D` chord:

```
B{G}ye bye Li'l Seb{C}astin {Am}    {D}
```

## Other Scripts

There are a couple experimental scripts as well that are not supported

- `./gradlew txttoyml -Psong=chords.txt` - Convert a specific format of txt file to the yml format. The chord lines should start with a `$` character
- `./gradlew rename -Psong=song.yml` - Make sure the yml song is named in a consistent way

## References
- https://en.wikipedia.org/wiki/EPUB
- https://github.com/mattharrison/epub-css-starter-kit

