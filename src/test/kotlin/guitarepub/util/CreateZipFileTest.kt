package guitarepub.util

import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertTrue

import net.lingala.zip4j.ZipFile

class CreateZipFileTest {
    @Test fun testAddFolderToZip() {
        val dir = createFilesToZip()

        val outputDir = createTempDir("guitar-unittest-output")
        val fileName = "test.zip"

        CreateZipFile().addFolderToZip(dir, outputDir.absolutePath, fileName)

        //unzip and verify
        val verifyDir = createTempDir("guitar-unittest-verify")
        val zipFile = ZipFile("${outputDir.absolutePath}/test.zip")
        zipFile.extractAll(verifyDir.absolutePath)

        verifyFilesExist(verifyDir)
    }

    private fun createFilesToZip(): File {
        val rootDir = createTempDir("guitar-unittest")
        File(rootDir, "file.txt").writeText("File Contents")

        val subDir = File(rootDir, "subdir")
        subDir.mkdirs()
        File(subDir, "subdir-file.txt").writeText("More File Contents")

        return rootDir
    }

    private fun verifyFilesExist(verifyDir: File) { 
        //root file should be there
        assertTrue(File(verifyDir, "file.txt").exists())

        //subdir should be there
        val verifySubDir = File(verifyDir, "subdir")
        assertTrue(verifySubDir.exists())
        assertTrue(verifySubDir.isDirectory())

        //subdir file should be there
        assertTrue(File(verifySubDir, "subdir-file.txt").exists())
    }
}