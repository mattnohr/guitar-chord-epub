package guitarepub.util

import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertEquals

class StringReaderTest {
    val classUnderTest = SongReader()

    @Test fun testReadWithoutCapo() {
        val fileName = createYamlFile(getSimpleYaml()).getAbsolutePath()

        val song = classUnderTest.readSong(fileName)
        assertEquals("Baby One More Time", song.title)
        assertEquals("Britney Spears", song.artist)
        assertEquals(null, song.capo)
    }

    @Test fun testReadWithCapo() {
        val fileName = createYamlFile(getYamlWithCapo()).getAbsolutePath()

        val song = classUnderTest.readSong(fileName)
        assertEquals("Baby One More Time", song.title)
        assertEquals("Britney Spears", song.artist)
        assertEquals("3", song.capo)
    }

    @Test fun testReadOutOfOrder() {
        val fileName = createYamlFile(getYamlOutOfOrder()).getAbsolutePath()

        val song = classUnderTest.readSong(fileName)
        assertEquals("Baby One More Time", song.title)
        assertEquals("Britney Spears", song.artist)
        assertEquals("3", song.capo)
    }

    @Test fun testReadWithoutDocStart() {
        val fileName = createYamlFile(getYamlWithoutDocStart()).getAbsolutePath()

        val song = classUnderTest.readSong(fileName)
        assertEquals("Baby One More Time", song.title)
        assertEquals("Britney Spears", song.artist)
    }

    @Test fun testAlbumAndTrack() {
        val fileName = createYamlFile(getYamlWithAlbumAndTrack()).getAbsolutePath()

        val song = classUnderTest.readSong(fileName)
        assertEquals("The Singles Collection", song.album)
        assertEquals("2", song.track)
    }

    @Test fun testTranspose() {
        val fileName = createYamlFile(getYamlWithTranspose()).getAbsolutePath()

        val song = classUnderTest.readSong(fileName)
        assertEquals(-2, song.transpose)
    }

    private fun createYamlFile(contents: String) : File {
        val yaml = createTempFile("tmp", ".yml")
        yaml.writeText(contents)
        return yaml
    }

    private fun getSimpleYaml() : String {
        return """
        ---
        title: Baby One More Time
        artist: Britney Spears
        chords: |
            O{Am}h baby, baby
            How w{E}as I supposed to k{C}now
        """.trimIndent()
    }

    private fun getYamlWithCapo() : String {
        return """
        ---
        title: Baby One More Time
        artist: Britney Spears
        capo: 3
        chords: |
            O{Am}h baby, baby
            How w{E}as I supposed to k{C}now
        """.trimIndent()
    }

    private fun getYamlOutOfOrder() : String {
        return """
        ---
        capo: 3
        artist: Britney Spears
        chords: |
            O{Am}h baby, baby
            How w{E}as I supposed to k{C}now
        title: Baby One More Time
        """.trimIndent()
    }

    private fun getYamlWithoutDocStart() : String {
        //no --- at the start
        return """
        title: Baby One More Time
        artist: Britney Spears
        chords: |
            O{Am}h baby, baby
            How w{E}as I supposed to k{C}now
        """.trimIndent()
    }

    private fun getYamlWithAlbumAndTrack() : String {
        return """
        ---
        title: Baby One More Time
        artist: Britney Spears
        album: The Singles Collection
        track: 2
        chords: |
            O{Am}h baby, baby
            How w{E}as I supposed to k{C}now
        """.trimIndent()
    }

    private fun getYamlWithTranspose() : String {
        return """
        ---
        title: Baby One More Time
        artist: Britney Spears
        transpose: -2
        chords: |
            O{Am}h baby, baby
            How w{E}as I supposed to k{C}now
        """.trimIndent()
    }
}