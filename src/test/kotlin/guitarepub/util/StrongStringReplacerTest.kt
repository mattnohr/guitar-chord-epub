package guitarepub.util

import guitarepub.domain.Song

import kotlin.test.Test
import kotlin.test.assertEquals

class StrongStringReplacerTest {
    val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain")
    val classUnderTest = SongStringReplacer()

    @Test fun testReplaceSongArtist() {
        val inputLines = listOf("Test {{songArtist}}")
        val outputLines = classUnderTest.replaceStringsInTemplate(inputLines, song)

        assertEquals(outputLines.size, 1)
        assertEquals(outputLines.get(0), "Test Prince")
    }

    @Test fun testReplaceSongId() {
        val inputLines = listOf("Test {{songId}}")
        val outputLines = classUnderTest.replaceStringsInTemplate(inputLines, song)

        assertEquals(outputLines.size, 1)
        assertEquals(outputLines.get(0), "Test ${song.id}")
    }

    @Test fun testReplaceSongTitle() {
        val inputLines = listOf("Test {{songTitle}}")
        val outputLines = classUnderTest.replaceStringsInTemplate(inputLines, song)

        assertEquals(outputLines.size, 1)
        assertEquals(outputLines.get(0), "Test Purple Rain")
    }

    @Test fun testReplaceMultipleLines() {
        val inputLines = listOf("Test {{songArtist}}", "Test {{songTitle}}")
        val outputLines = classUnderTest.replaceStringsInTemplate(inputLines, song)

        assertEquals(outputLines.size, 2)
        assertEquals(outputLines.get(0), "Test Prince")
        assertEquals(outputLines.get(1), "Test Purple Rain")
    }

    @Test fun testReplaceLineWithoutKey() {
        val inputLines = listOf("Test without key")
        val outputLines = classUnderTest.replaceStringsInTemplate(inputLines, song)

        assertEquals(outputLines.size, 1)
        assertEquals(outputLines.get(0), "Test without key")
    }
}