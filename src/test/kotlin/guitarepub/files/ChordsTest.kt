package guitarepub.files

import guitarepub.domain.ChordName
import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ChordsTest {

    @Test fun testCreatingFile() {
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain\nPurple Rain")
        val testDir = createTempDir("guitar-unittest")
        val classUnderTest = Chords(testDir, song)

        classUnderTest.createChordFile()

        //make sure the file exists
        val outputFile = File(testDir, "chords.xhtml")
        assertTrue(outputFile.exists())

        val outputLines = outputFile.readLines()
        assertEquals("<title>Purple Rain</title>", outputLines.get(5).trim())
        assertEquals("<h1 class=\"title\">Purple Rain</h1>", outputLines.get(10).trim())
        assertEquals("<div class=\"artist\">Prince</div>", outputLines.get(11).trim())

        //chords
        assertEquals("<p class=\"musicLineWithChords\">", outputLines.get(14).trim())
        assertEquals("Purple Rain Purple R<span class=\"chord chord1\">C</span>ain", outputLines.get(15).trim())
        assertEquals("</p>", outputLines.get(16).trim())
        assertEquals("<p class=\"musicLine\">", outputLines.get(17).trim())
        assertEquals("Purple Rain", outputLines.get(18).trim())
        assertEquals("</p>", outputLines.get(19).trim())
    }

    @Test fun testTransposeZero() {
        val transpose = 0
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain", "", "", transpose)
        val testDir = createTempDir("guitar-unittest")
        val classUnderTest = Chords(testDir, song)

        val inputChord = "C"
        val outputChord = classUnderTest.transposeChord(inputChord)

        assertEquals(inputChord, outputChord)
    }

    @Test fun testTransposeChordsUp() {
        val transpose = 2
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain", "", "", transpose)
        val testDir = createTempDir("guitar-unittest")
        val classUnderTest = Chords(testDir, song)

        var inputChord = "C"
        var outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("D", outputChord)

        inputChord = "C#"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Eb", outputChord)

        inputChord = "G#"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Bb", outputChord)

        inputChord = "Ab"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Bb", outputChord)

        inputChord = "Cm7"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Dm7", outputChord)
    }

    @Test fun testTransposeChordsDown() {
        val transpose = -2
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain", "", "", transpose)
        val testDir = createTempDir("guitar-unittest")
        val classUnderTest = Chords(testDir, song)

        var inputChord = "C"
        var outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Bb", outputChord)

        inputChord = "A#"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Ab", outputChord)

        inputChord = "A"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("G", outputChord)

        inputChord = "Bb"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Ab", outputChord)

        inputChord = "Cm7"
        outputChord = classUnderTest.transposeChord(inputChord)
        assertEquals("Bbm7", outputChord)
    }

    @Test fun testSplitChords() {
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain")
        val testDir = createTempDir("guitar-unittest")
        val classUnderTest = Chords(testDir, song)

        //simpliest chord
        var inputChord = "G"
        var expectedOutput = ChordName("G", "", "")
        var actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //with flat
        inputChord = "Bb"
        expectedOutput = ChordName("Bb", "", "")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //with sharp
        inputChord = "C#"
        expectedOutput = ChordName("C#", "", "")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //minor chord
        inputChord = "Am"
        expectedOutput = ChordName("A", "m", "")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //minor flat chord
        inputChord = "Abm"
        expectedOutput = ChordName("Ab", "m", "")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //minor sharp chord
        inputChord = "C#m"
        expectedOutput = ChordName("C#", "m", "")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //7 chord
        inputChord = "D7"
        expectedOutput = ChordName("D", "", "7")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //minor 7 chord
        inputChord = "Am7"
        expectedOutput = ChordName("A", "m", "7")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)

        //sharp minor 7 chord
        inputChord = "G#m7"
        expectedOutput = ChordName("G#", "m", "7")
        actualOutput = classUnderTest.getChordParts(inputChord)
        assertEquals(expectedOutput, actualOutput)
    }

    @Test fun testGetChordStyle() {
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain")
        val testDir = createTempDir("guitar-unittest")
        val classUnderTest = Chords(testDir, song)

        assertEquals("chord chord1", classUnderTest.getChordStyle("G"))
        assertEquals("chord chord2", classUnderTest.getChordStyle("G7"))
        assertEquals("chord chord3", classUnderTest.getChordStyle("Am7"))
        assertEquals("chord chord4", classUnderTest.getChordStyle("Bbm7"))
        assertEquals("chord chord5", classUnderTest.getChordStyle("Fmaj7"))

        //this is 6 long, but default anything longer to 5
        assertEquals("chord chord5", classUnderTest.getChordStyle("F#maj7"))
    }
}