package guitarepub.files

import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TocNcxTest {
    /**
     * This test depends on the template file not changing. It has hard-coded 
     * line numbers throughout. This could be cleaned up if the template file 
     * ever needs to change.
     */
    @Test fun testCreatingFile(){
        val testDir = createTempDir("guitar-unittest")
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain")

        TocNcx(testDir, song).createTocFile()
        
        //does the file exist?
        val outputFile = File(testDir, "toc.ncx")
        assertTrue(outputFile.exists())
        
        //is the file the right number of lines?
        val outputLines = outputFile.readLines()
        assertEquals(32, outputLines.size)

        //make sure the replacements worked
        var replacedLine = outputLines.get(10).trim()
        var replacedContent = replacedLine.substring(0, replacedLine.indexOf(" <!--"))
        assertEquals("<meta name=\"dtb:uid\" content=\"${song.id}\"/>", replacedContent)

        replacedLine = outputLines.get(17).trim()
        assertEquals("<text>Purple Rain</text>", replacedLine)

        replacedLine = outputLines.get(21).trim()
        assertEquals("<text>Prince</text>", replacedLine)
    }
}