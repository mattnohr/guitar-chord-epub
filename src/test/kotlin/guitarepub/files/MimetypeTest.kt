package guitarepub.files

import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MimetypeTest {
    @Test fun testCreatingFile(){
        val testDir = createTempDir("guitar-unittest")

        Mimetype(testDir).copyMimetypeFile()
        
        //does the file exist?
        val outputFile = File(testDir, "mimetype")
        assertTrue(outputFile.exists())
        
        //is the file the right number of lines?
        val outputLines = outputFile.readLines()
        assertEquals(1, outputLines.size)
    }
}