package guitarepub.files

import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ContainerXMLTest {
    @Test fun testCreatingFile(){
        val testDir = createTempDir("guitar-unittest")

        ContainerXml(testDir).copyContainerXmlFile()
        
        //does the new directory exist?
        val outputDir = File(testDir, "META-INF")
        assertTrue(outputDir.exists())
        assertTrue(outputDir.isDirectory())

        //does the new file exist?
        val outputFile = File(outputDir, "container.xml")
        assertTrue(outputDir.exists())
        
        //is the file the right number of lines?
        val outputLines = outputFile.readLines()
        assertEquals(6, outputLines.size)
    }
}