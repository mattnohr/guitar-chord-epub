package guitarepub.files

import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ContentOpfTest {
    /**
     * This test depends on the template file not changing. It has hard-coded 
     * line numbers throughout. This could be cleaned up if the template file 
     * ever needs to change.
     */
    @Test fun testCreatingFile(){
        val testDir = createTempDir("guitar-unittest")
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain")

        ContentOpf(testDir, song).createContentFile()
        
        //does the file exist?
        val outputFile = File(testDir, "content.opf")
        assertTrue(outputFile.exists())
        
        //is the file the right number of lines?
        val outputLines = outputFile.readLines()
        assertEquals(27, outputLines.size)

        //make sure the replacements worked
        var replacedLine = outputLines.get(7).trim()
        assertEquals("<dc:creator opf:file-as=\"Prince\" opf:role=\"aut\">Prince</dc:creator>", replacedLine)

        replacedLine = outputLines.get(4).trim()
        assertEquals("<dc:title>Purple Rain</dc:title>", replacedLine)

        replacedLine = outputLines.get(6).trim()
        assertEquals("<dc:identifier id=\"uuid_id\" opf:scheme=\"uuid\">urn:uuid:${song.id}</dc:identifier>", replacedLine)

        //album and track should be blank lines
        replacedLine = outputLines.get(8).trim()
        assertEquals("", replacedLine)
        
        replacedLine = outputLines.get(9).trim()
        assertEquals("", replacedLine)
    }

    @Test fun testAlbumAndTrack() {
        val testDir = createTempDir("guitar-unittest")
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain", "The Very Best of Prince", "6")

        ContentOpf(testDir, song).createContentFile()
        val outputLines = File(testDir, "content.opf").readLines()

        var replacedLine = outputLines.get(8).trim()
        assertEquals("<meta content=\"The Very Best of Prince\" name=\"calibre:series\"/>", replacedLine)

        replacedLine = outputLines.get(9).trim()
        assertEquals("<meta content=\"6\" name=\"calibre:series_index\"/>", replacedLine)
    }

    @Test fun testAlbumAndTrackNull() {
        val testDir = createTempDir("guitar-unittest")
        val song = Song("Purple Rain", "Prince", "3", "Purple Rain Purple R{C}ain", "null", "null")

        ContentOpf(testDir, song).createContentFile()
        val outputLines = File(testDir, "content.opf").readLines()

        var replacedLine = outputLines.get(8).trim()
        assertEquals("", replacedLine)

        replacedLine = outputLines.get(9).trim()
        assertEquals("", replacedLine)
    }
}