package guitarepub.files

import guitarepub.domain.Song

import java.io.File

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CssFilesTest {
    @Test fun testMovingFiles(){
        val testDir = createTempDir("guitar-unittest")

        CssFiles(testDir).copyCssFiles()
        
        //does the new directory exist?
        val outputDir = File(testDir, "css")
        assertTrue(outputDir.exists())
        assertTrue(outputDir.isDirectory())

        //check the new files exist
        val filesInDir = outputDir.listFiles()
        assertEquals(5, filesInDir.size)

        listOf("base.css", "main.css", //css
            "MgOpenModataRegular.ttf", "invisible1.ttf", "Inconsolata-Regular.otf" //referenced fonts
            ).forEach {
                val nextFile = File(outputDir, it)
                assertTrue(nextFile.exists())
        }
    }
}