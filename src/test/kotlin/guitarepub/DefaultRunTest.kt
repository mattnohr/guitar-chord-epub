package guitarepub

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DefaultRunTest {
    @Test fun testDefaultMessage() {
        val message = DefaultRun().getMessage()
        val lines = message.split("\n")

        assertEquals(4, lines.size)

        //make sure it at least mentions the command line params
        listOf("-Psong", "-Poutput").forEach {
            assertTrue(message.indexOf(it) > 0)
        }
    }
}