package guitarepub

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TextToYamlTest {
    val classUnderTest = TextToYaml()

    @Test fun testSimpleLines() {
        val inputLines = listOf(
            "", 
            "  ", 
            "test")
        val outputLines = classUnderTest.convertLines(inputLines)

        assertEquals(3, outputLines.size)
        assertEquals("", outputLines[0])
        assertEquals("  ", outputLines[1])
        assertEquals("test", outputLines[2])
    }

    @Test fun testSingleReplacement() {
        val inputLines = listOf(
            "\$E", 
            "These Are Lyrics")
        val outputLines = classUnderTest.convertLines(inputLines)

        assertEquals(1, outputLines.size)
        assertEquals("T{E}hese Are Lyrics", outputLines[0])
    }

    @Test fun testMultipleChordsOneLine() {
        val inputLines = listOf(
            "\$E     A", 
            "These Are Lyrics")
        val outputLines = classUnderTest.convertLines(inputLines)

        assertEquals(1, outputLines.size)
        assertEquals("T{E}hese A{A}re Lyrics", outputLines[0])
    }

    @Test fun testLongerChordName() {
        val inputLines = listOf(
            "\$Em", 
            "These Are Lyrics")
        val outputLines = classUnderTest.convertLines(inputLines)

        assertEquals(1, outputLines.size)
        assertEquals("T{Em}hese Are Lyrics", outputLines[0])
    }

    @Test fun testChordLineStartsWithSpaces() {
        val inputLines = listOf(
            "\$      D7", 
            "These Are Lyrics")
        val outputLines = classUnderTest.convertLines(inputLines)

        assertEquals(1, outputLines.size)
        assertEquals("These A{D7}re Lyrics", outputLines[0])
    }

    @Test fun testMultilineReplacements() {
        val inputLines = listOf(
            "\$Em   Am7", 
            "Line One", 
            "\$G", 
            "Line Two")
        val outputLines = classUnderTest.convertLines(inputLines)

        assertEquals(2, outputLines.size)
        assertEquals("L{Em}ine O{Am7}ne", outputLines[0])
        assertEquals("L{G}ine Two", outputLines[1])
    }
}