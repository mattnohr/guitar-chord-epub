package guitarepub.domain

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ChordNameTest {
    @Test fun testIsSharp() {
        var chordName = ChordName("G", "", "")
        assertFalse(chordName.isSharp())

        chordName = ChordName("Gb", "", "")
        assertFalse(chordName.isSharp())

        chordName = ChordName("G#", "", "")
        assertTrue(chordName.isSharp())
    }

    @Test fun testIsFlat() {
        var chordName = ChordName("G", "", "")
        assertFalse(chordName.isFlat())

        chordName = ChordName("G#", "", "")
        assertFalse(chordName.isFlat())

        chordName = ChordName("Gb", "", "")
        assertTrue(chordName.isFlat())
    }

    @Test fun testKeyToFlat() {
        var chordName = ChordName("G", "", "")
        assertEquals("G", chordName.keyAsFlat())

        chordName = ChordName("Gb", "", "")
        assertEquals("Gb", chordName.keyAsFlat())

        chordName = ChordName("G#", "", "")
        assertEquals("Ab", chordName.keyAsFlat())

        //not a chord that would be used/covered
        chordName = ChordName("B#", "", "")
        assertEquals("Unknown", chordName.keyAsFlat())
    }

    @Test fun testGetKey() {
        //dumb test, but it was never tested/used
        var chordName = ChordName("G", "", "")
        assertEquals("G", chordName.key)
    }
}