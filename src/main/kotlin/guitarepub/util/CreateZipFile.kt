package guitarepub.util

import java.io.File

import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.model.ZipParameters

/**
 * This is a generic way to create a zip file with all the contents of a folder
 */
class CreateZipFile() {
    fun addFolderToZip(folderToZip: File, destination: String, zipFileName: String) {
        val zipParameters = ZipParameters()
        zipParameters.setIncludeRootFolder(false)
        ZipFile("${destination}/${zipFileName}").addFolder(folderToZip, zipParameters)
    }
}