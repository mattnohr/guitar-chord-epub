package guitarepub.util

import guitarepub.domain.Song

class SongStringReplacer() {
    fun replaceStringsInTemplate(inputLines: List<String>, song: Song) : MutableList<String> {
        val outputLines = mutableListOf<String>()
        inputLines.forEach { nextLine ->
            var nextLineOutput = nextLine
            nextLineOutput = nextLineOutput.replace("{{songId}}", song.id.toString(), true)
            nextLineOutput = nextLineOutput.replace("{{songTitle}}", song.title, true)
            nextLineOutput = nextLineOutput.replace("{{songArtist}}", song.artist, true)
    
            outputLines.add(nextLineOutput)
        }
    
        return outputLines
    } 
}