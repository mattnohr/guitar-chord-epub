package guitarepub.util

import guitarepub.domain.Song

import java.io.File

import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor

class SongReader() {
    fun readSong(fileName: String) : Song {
        val yaml = Yaml(Constructor(Song::class.java))
        val inputStream = File(fileName).inputStream()
        return yaml.load(inputStream) as Song
    }
}