package guitarepub

import guitarepub.domain.Song
import guitarepub.util.SongReader

import java.io.File

class TextToYaml() {
    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            val song = args[0]
            TextToYaml().convertToYaml(song)
        }
    }

    fun convertToYaml(songPath: String) {
        val inputFile = File(songPath)
        
        val inputLines = inputFile.readLines()
        val outputLines = convertLines(inputLines)

        println(outputLines.joinToString("\n"))
    }

    fun convertLines(inputLines: List<String>) : List<String> {
        var lineIndex = 0

        val outputLines = mutableListOf<String>()

        while(lineIndex < inputLines.size) {
            if(inputLines[lineIndex].startsWith("$")) {
                val chordLine = inputLines[lineIndex].substring(1) //start at one to skip $
                val lyricLine = inputLines[lineIndex+1]

                val chordList = chordLine.trim().split("\\s+".toRegex())
                //println("list = $chordList")

                var lyricIndex = 0
                var chordLineIndex = 0
                var chordListIndex = 0
                val embeddedLine = StringBuilder()

                while(lyricIndex < lyricLine.length) {
                    val lyricLetter = lyricLine.substring(lyricIndex, lyricIndex + 1)
                    embeddedLine.append(lyricLetter)

                    if(chordLine.length > lyricIndex && chordLineIndex == lyricIndex) {
                        val chordLetter = chordLine.substring(lyricIndex, lyricIndex + 1)
                        if(chordLetter == " ") {
                            //do nothing
                            chordLineIndex++
                        } else {
                            //find the full chord
                            val fullChord = chordList[chordListIndex]
                            embeddedLine.append("{${fullChord}}")
                            
                            chordListIndex++
                            chordLineIndex += fullChord.length
                        }
                    }
                    lyricIndex++

                }

                outputLines.add(embeddedLine.toString())

                lineIndex += 2
                
            } else {
                outputLines.add(inputLines[lineIndex])

                lineIndex++
            }
        }

        return outputLines
    }
}