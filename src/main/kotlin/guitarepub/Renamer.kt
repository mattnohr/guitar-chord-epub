package guitarepub

import guitarepub.domain.Song
import guitarepub.util.SongReader

import java.io.File

class Renamer() {
    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            val song = args[0]
            Renamer().renameFile(song)
        }
    }

    fun renameFile(songPath: String) {
        val song = SongReader().readSong(songPath)

        val inputFile = File(songPath)
        val outputFile = File("output", "${song.artist} - ${song.title}.yml")

        if(outputFile.exists()) {
            outputFile.delete()
        }

        inputFile.copyTo(outputFile)

        println("Renamed $songPath to output/${outputFile.name}")
    }
}