package guitarepub

class DefaultRun() {
    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            println(DefaultRun().getMessage())
        }
    }
    
    fun getMessage() : String {
        return """
            This is the default execution for the 'gradle run' command
            To generate an ebook, use:
                ./gradlew epub -Psong=path/to/song.yml
            You can optionally pass -Poutput=path/to/output/directory
        """.trimIndent()
    }
}