package guitarepub.files

import guitarepub.domain.Song
import guitarepub.util.SongStringReplacer

import java.io.File

class TocNcx(contentDirectory: File, song: Song) {
    val contentDirectory = contentDirectory
    val song = song

    fun createTocFile() {
        //read contents of template file
        val templateFile = File("src/templates/OEBPS/toc.ncx")

        //replace strings
        val inputLines = templateFile.readLines()
        val outputLines = SongStringReplacer().replaceStringsInTemplate(inputLines, song)

        //write file to temp directory
        val destinationFile = File(contentDirectory, "toc.ncx")
        destinationFile.writeText(outputLines.joinToString("\n"))
    }
}