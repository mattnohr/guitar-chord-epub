package guitarepub.files

import java.io.File

class CssFiles(contentDirectory: File) {
    val contentDirectory = contentDirectory

    fun copyCssFiles() {
        val cssDir = File("${contentDirectory.absolutePath}/css/")
        cssDir.mkdirs()

        listOf("base.css", "main.css", //css
            "MgOpenModataRegular.ttf", "invisible1.ttf", "Inconsolata-Regular.otf" //referenced fonts
            ).forEach {
            val templateFile = File("src/templates/OEBPS/css/${it}")
            val destinationFile = File(cssDir, it)
            templateFile.copyTo(destinationFile)
        }
    }
}