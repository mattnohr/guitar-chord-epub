package guitarepub.files

import guitarepub.domain.Song
import guitarepub.util.SongStringReplacer

import java.io.File

class ContentOpf(contentDirectory: File, song: Song) {
    val contentDirectory = contentDirectory
    val song = song

    fun createContentFile() {
         //read contents of template file
        val templateFile = File("src/templates/OEBPS/content.opf")

        //replace common strings
        val inputLines = templateFile.readLines()
        val outputLines = SongStringReplacer().replaceStringsInTemplate(inputLines, song)

        //replace optional album string
        val albumIndex = outputLines.indexOf("    {{songAlbum}}")
        if(song.album == null || song.album.equals("null")) {
            outputLines.set(albumIndex, "")
        } else {
            outputLines.set(albumIndex, "<meta content=\"${song.album}\" name=\"calibre:series\"/>")
        }

        //replace optional track string
        val trackIndex = outputLines.indexOf("    {{songTrack}}")
        if(song.track == null || song.track.equals("null")) {
            outputLines.set(trackIndex, "")
        } else {
            outputLines.set(trackIndex, "<meta content=\"${song.track}\" name=\"calibre:series_index\"/>")
        }

        //write file to temp directory
        val destinationFile = File(contentDirectory, "content.opf")
        destinationFile.writeText(outputLines.joinToString("\n"))

    }

}