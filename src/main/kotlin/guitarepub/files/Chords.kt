package guitarepub.files

import guitarepub.domain.ChordName
import guitarepub.domain.Song
import guitarepub.util.SongStringReplacer

import java.io.File
import java.util.regex.Matcher
import java.util.regex.Pattern

class Chords(contentDirectory: File, song: Song) {
    val contentDirectory = contentDirectory
    val song = song

    fun createChordFile() {
        //read contents of template file
        val templateFile = File("src/templates/OEBPS/chords.xhtml")

        //replace common strings
        val inputLines = templateFile.readLines()
        val outputLines = SongStringReplacer().replaceStringsInTemplate(inputLines, song)

        //replace capo if needed
        val capoIndex = outputLines.indexOf("    {{songCapo}}")
        if(song.capo == null || song.capo.equals("null")) {
            outputLines.set(capoIndex, "")
        } else {
            outputLines.set(capoIndex, "<div class=\"capo\">Capo: ${song.capo}</div>")
        }

        //replace song contents
        val songIndex = outputLines.indexOf("      {{songChords}}")
        outputLines.set(songIndex, buildChordHtml(song))

        //TODO temp output file contents
        //outputLines.forEach { println(it) }

        //write file to temp directory
        val destinationFile = File(contentDirectory, "chords.xhtml")
        destinationFile.writeText(outputLines.joinToString("\n"))
    }

    fun buildChordHtml(song: Song) : String {
        val inputLines = song.chords.split("\n")
        val outputLines = mutableListOf<String>()

        inputLines.forEach{ nextLine ->
            if(nextLine.indexOf("{") >= 0) {
                //this line has chords in it
                outputLines.add("<p class=\"musicLineWithChords\">")

                var formattedLine = nextLine.replace("  ", "&nbsp;&nbsp;", true)

                var nextChordStartIndex = formattedLine.indexOf("{")
                var nextChordEndIndex : Int
                var nextChord : String
                val musicLineBuilder = StringBuilder()


                //println("Next line: $formattedLine" )
                musicLineBuilder.append(formattedLine.substring(0, nextChordStartIndex))
                while(nextChordStartIndex > 0) {
                    nextChordEndIndex = formattedLine.indexOf("}", nextChordStartIndex)
                    nextChord = formattedLine.substring(nextChordStartIndex + 1, nextChordEndIndex)

                    val transposed = transposeChord(nextChord)
                    //println("Found chord: $nextChord")

                    musicLineBuilder.append("<span class=\"${getChordStyle(transposed)}\">")
                    musicLineBuilder.append(transposed)
                    musicLineBuilder.append("</span>")

                    //update indicies
                    nextChordStartIndex = formattedLine.indexOf("{", nextChordEndIndex)

                    //add more "non-chord" text
                    if(nextChordStartIndex > 0){
                        //there is another chord coming
                        musicLineBuilder.append(formattedLine.substring(nextChordEndIndex + 1, nextChordStartIndex))
                    } else {
                        //this is the last chord
                        musicLineBuilder.append(formattedLine.substring(nextChordEndIndex + 1 ))
                    }
                }
                //println("Output line: ${musicLineBuilder.toString()}")
                
                outputLines.add(musicLineBuilder.toString())
                outputLines.add("</p>")
            } else {
                outputLines.add("<p class=\"musicLine\">")
                if(nextLine.trim().equals("")) {
                    outputLines.add("&nbsp;")
                } else {
                    outputLines.add(nextLine)
                }
                outputLines.add("</p>")
            }
        }

        return outputLines.joinToString("\n")
    }   

    fun getChordStyle(chordName: String): String {
        if(chordName.length == 1) {
            //ex: C or G
            return "chord chord1"
        } else if(chordName.length == 2) {
            //ex: Em or D7
            return "chord chord2"
        } else if(chordName.length == 3) {
            //ex: Am7
            return "chord chord3"
        } else if(chordName.length == 4) {
            return "chord chord4"
        } else {
            //ex: Fmaj7
            //if anything longer it will fall into here as well
            return "chord chord5"
        }
    }

    fun transposeChord(originalChord: String) : String {
        //println("Tranposing ${originalChord} - ${song.transpose}")

        if(song.transpose == 0) {
            //println("Not transposing")
            return originalChord
        } else {
            val chordName = getChordParts(originalChord)
            val keyList = listOf("A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab")
            val keyIndex = keyList.indexOf(chordName.keyAsFlat())
            
            var newIndex = keyIndex + song.transpose
            if(newIndex >= keyList.size) {
                newIndex -= keyList.size
            } else if (newIndex < 0) {
                newIndex += keyList.size
            }

            return "${keyList.get(newIndex)}${chordName.minor}${chordName.augment}"
        }
    }

    fun getChordParts(originalChord: String) : ChordName {
        val pattern = Pattern.compile("([A-G][#|b]?)([m]?)([2-9]?)")
        val matcher = pattern.matcher(originalChord)
        
        //TODO what to do if the pattern isn't found?
        //if(matcher.find()) {
        matcher.find()
        val chordName = ChordName(matcher.group(1), matcher.group(2), matcher.group(3))
        return chordName
    }
}