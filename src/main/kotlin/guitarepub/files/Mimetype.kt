package guitarepub.files

import java.io.File

class Mimetype(rootDirectory: File) {
    val rootDirectory = rootDirectory

    fun copyMimetypeFile() {
        val templateFile = File("src/templates/mimetype")
        val destinationFile = File(rootDirectory, "mimetype")
        templateFile.copyTo(destinationFile)
    }
}