package guitarepub.files

import java.io.File

class ContainerXml(rootDirectory: File) {
    val rootDirectory = rootDirectory

    fun copyContainerXmlFile() {
        val metaDir = File("${rootDirectory.absolutePath}/META-INF/")
        metaDir.mkdirs()

        val templateFile = File("src/templates/META-INF/container.xml")
        val destinationFile = File(metaDir, "container.xml")
        templateFile.copyTo(destinationFile)
    }
}