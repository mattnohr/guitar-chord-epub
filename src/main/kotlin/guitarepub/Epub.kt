package guitarepub

import guitarepub.domain.Song
import guitarepub.files.*
import guitarepub.util.CreateZipFile
import guitarepub.util.SongReader

import java.io.File

class Epub() {
    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            val song = args[0]
            println("Creating ebook for $song")
            Epub().createEpub(song, args[1])
        }
    }

    fun createEpub(songPath: String, outputPath: String) {
        val song = SongReader().readSong(songPath)

        createEbook(song, outputPath)
    }
    
    private fun createEbook(song: Song, outputPath: String) {
        //create a temporary directory to hold all the information
        val rootDir = createTempDir("guitarbook")
        println("Building ebook contents in ${rootDir.absolutePath}")
    
        //create file at root of directory
        Mimetype(rootDir).copyMimetypeFile()
    
        //create container.xml file in META-INF directory
        ContainerXml(rootDir).copyContainerXmlFile()
    
        //create OEBPS directory with files
        createOEBPSDir(rootDir, song)
    
        //create zip/epub file
        val fileName = "${song.artist} - ${song.title}.epub"
        CreateZipFile().addFolderToZip(rootDir, outputPath, fileName)
        println("Created ebook in output/${fileName}")
    }
    
    //OEBPS
    private fun createOEBPSDir(rootDir: File, song: Song) {
        val oebpsDir = File("${rootDir.absolutePath}/OEBPS/")
        oebpsDir.mkdirs()
    
        //copy CSS files
        CssFiles(oebpsDir).copyCssFiles()
    
        //create main file
        Chords(oebpsDir, song).createChordFile()
    
        //create other files
        ContentOpf(oebpsDir, song).createContentFile()
        TocNcx(oebpsDir, song).createTocFile()
    }
}