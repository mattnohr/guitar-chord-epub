package guitarepub.domain

data class ChordName(
    val key: String, 
    val minor: String,
    val augment: String) {

    fun isSharp(): Boolean {
        return key.indexOf("#") > 0
    }

    fun isFlat(): Boolean {
        return key.indexOf("b") > 0
    }
 
    fun keyAsFlat(): String {
        if(isSharp()) {
            val sharpToFlatMap = mapOf(
                "A#" to "Bb", 
                "C#" to "Db",
                "D#" to "Eb",
                "F#" to "Gb",
                "G#" to "Ab")
            return sharpToFlatMap.get(key) ?: "Unknown"
        } else {
            return key
        }
    }
}