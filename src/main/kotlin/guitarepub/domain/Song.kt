package guitarepub.domain

import java.util.UUID

data class Song(
    var title: String = "", 
    var artist: String = "", 
	var capo: String? = null,
    var chords: String = "",
    var album: String? = null,
    var track: String? = null,
    var transpose: Int = 0,
    val id: UUID = UUID.randomUUID()
)